
/*!
 * \brief brief
  \detailed Some examples of what can be used for comments:
  
  ### Making lists:
  Lists are made using hyphens( -item1, -item2 or numbers (1. item1 2. item2). Nested items are made using an extra tab and a + sign
  - Item 1
  - Item 2
    + nested item
    + nested item
  - Item 3
  or a numbered list:
  1. Item 1
     + nested item
  + nested item
  2. Item 2

*/

class example_class
{
  public:
    //! Simple description: An enum.
    /*! More detailed enum description. */
    enum TEnum { 
                 TVal1, /*!< Enum value TVal1. */  
                 TVal2, /*!< Enum value TVal2. */  
                 TVal3  /*!< Enum value TVal3. */  
               } 
         //! Enum pointer.
         /*! Details. */
         *enumPtr, 
         //! Enum variable.
         /*! Details. */
         enumVar;  
    
    //! A constructor.
    /*!
      We can give descriptions of input paratmeters inside the function with as done here
    */
    
    example_class(int a /*!< Description of a */, char b/*!< Description of b */);
    
    //! A destructor.
    /*!
      A more elaborate description of the destructor.
    */
   ~example_class();
    
    //! A normal member taking two arguments and returning an integer value.
    /*!
      Or we can define them outside the function using doxygen commands like `\param` 
      \param a an integer argument.
      \param s a constant character pointer.
      \return The test results
      \sa example_class(), example_class(), testMeToo() and publicVar()
    */
    int testMe(int a,const char *s);
       
    //! A pure virtual member.
    /*!
      \sa testMe()
      \param c1 the first argument.
      \param c2 the second argument.
    */
    virtual void testMeToo(char c1,char c2) = 0;
   
    //! A public variable.
    /*!
      Details.
    */
    int publicVar;
       
    //! A function variable.
    /*!
      Details.
    */
    int (*handler)(int a,int b);
};
