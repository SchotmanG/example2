Landing Page
************************

COMPLEX TABLE:

+------------+------------+-----------+
| Header 1   | Header 2   | Header 3  |
+============+============+===========+
| body row 1 | column 2   | column 3  |
+------------+------------+-----------+
| body row 2 | Cells may span columns.|
+------------+------------+-----------+
| body row 3 | Cells may  | - Cells   |
+------------+ span rows. | - contain |
| body row 4 |            | - blocks. |
+------------+------------+-----------+

*SIMPLE TABLE:*

=====  =====  ======
   Inputs     Output
------------  ------
  A      B    A or B
=====  =====  ======
False  False  False
True   False  True
False  True   True
True   True   True
=====  =====  ======


H2 -- Page Sections
===================

**1. first things first**

	here a some indented text `hyper link <https://www.google.com>`_. 
	some more text

:doc:`classes`


here some non indented text

*2. seconds things second*
	you know whats up
	* item 1
	* item 2
	other type
	- item 1
	- item 2
	

H3 -- Subsection
----------------

.. csv-table:: Frozen Delights!
   :header: "Treat", "Quantity", "Description"
   :widths: 15, 10, 30

   "Albatross", 2.99, "On a stick!"
   "Crunchy Frog", 1.49, "If we took the bones out, it wouldn't be
   crunchy, now would it?"
   "Gannet Ripple", 1.99, "On a stick!"


H4 -- Subsubsection
+++++++++++++++++++

THE END


.. toctree::
   :hidden:

   self

.. toctree::
   :titlesonly:
   :glob:
   :maxdepth: 1
   :hidden:
   :caption: Modules:

   classes
   test
