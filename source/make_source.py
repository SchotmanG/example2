#  written by Jongkyu Kim (j.kim@fu-berlin.de)

#TRIGGERED BY FILE EXTENSIONS .hpp .cpp and .dox
import os
import sys

CAT_NAME = "Modules"
INDEX_TEMP = "./_index.rst"

def generateIndex(inDir, outDir, isTest ,testDir=" "):
    listModules = []
    for fileName in os.listdir(inDir) :
        if os.path.isdir( os.path.join(inDir, fileName)) == True:
            listModules.append(fileName)  
    listModules = sorted(listModules)

    # generate index.rst
    inFile = open(INDEX_TEMP, "r")
    outFile = open(os.path.join(outDir, "index.rst"), "w")
    for line in inFile :
        outFile.write(line)
    inFile.close()
    outFile.write("   :caption: %s:\n\n" % CAT_NAME)
    for moduleName in listModules :
        outFile.write("   %s\n" % (moduleName))
            

    #TEST RESULTS:
    #this writes the general test results which are in testDir and are converted to html in a test.rst file such that they are included in the test module.
    #todo: have test results on the main page of every part of code.
    if isTest:
        outFile.write("   test\n")
        outFile.close()
        #write test.rst
        outFile=open(os.path.join(outDir, "test.rst"), "w")
        #write title
        outFile.write("Test Results\n"+ "="*len("Test Results")+ "\n\n")
        #write the HTML files
        for file in os.listdir(testDir):
            if file.split(".")[-1] == "html":
                outFile.write(".. raw:: html\n   :file: %s\n\n" % (os.path.join(testDir, file)))

def generateRST(outDir, inDir, moduleName, listModules, listFiles) :
    if len(listModules) > 0 and os.path.isdir(outDir) == False:
        os.mkdir(outDir)


    outFile = open(outDir + ".rst","w")
    #label
    outFile.write(".. _%s-label:\n\n" % (moduleName))
    # title
    outFile.write(moduleName[0].upper() + moduleName[1:] + "\n")
    outFile.write("=" * len(moduleName) + "\n\n")

    #reference to anchors
    outFile.write("Table of Content: \n\n")
    
    for fileName in listFiles:
        outFile.write("- :ref:`%s-label`\n" % (fileName))
    outFile.write("\n")
    
    # doxygenfile
    for fileName in listFiles :
        #header +  label
        outFile.write(".. _%s-label:\n\n" % (fileName))
        outFile.write("%s \n" % (fileName))
        outFile.write("+"*len(fileName) + " \n\n") 

        #example code for when results have to be directly shown at the code explanation
        if os.path.exists(os.path.join(inDir, fileName.split(".")[0]+"-test-result.html")):
            
            outFile.write(".. raw:: html\n   :file: %s\n\n" % (os.path.join(inDir, fileName.split(".")[0]+"-test-result.html")))

        #input file
        outFile.write(".. doxygenfile:: %s\n" % (os.path.join(inDir, fileName)))
        outFile.write("   :project: myproject\n\n")

    # toctree
    outFile.write(".. toctree::\n")
    outFile.write("   :caption: %s:\n" % CAT_NAME)
    outFile.write("   :titlesonly:\n")
    outFile.write("   :maxdepth: 1\n")
    outFile.write("   :hidden:\n\n")
    for childModuleName in listModules :
       outFile.write("   %s/%s\n" % (moduleName, childModuleName) )
    outFile.close()

def generateRSTs(inDir, outDir,isRoot):
    listModules = []
    listFiles = []
    fileExtensions=["hpp", "cpp"] #THESE are seen as code and will be included
    for fileName in os.listdir(inDir) :
        if os.path.isdir( os.path.join(inDir,fileName)) == True:
            listModules.append(fileName)  
        else :
            fileExt = fileName.split(".")[-1]
            if fileExt in fileExtensions:
                listFiles.append(fileName)
    
    listModules = sorted(listModules)
    listFiles = sorted(listFiles)

    if isRoot == False :
        moduleName = outDir.split("/")[-1]
        generateRST(outDir, inDir, moduleName, listModules, listFiles)


    for moduleName in listModules :
        curInDir = os.path.join(inDir, moduleName)
        curOutDir = os.path.join(outDir, moduleName)
        generateRSTs(curInDir, curOutDir, False)

'''
Alphabet
========

.. doxygenfile:: alphabet.hpp
   :project: myproject

.. doxygenfile:: alphabet_container.hpp
   :project: myproject

.. doxygenfile:: compound_alphabet.hpp
   :project: myproject

.. toctree::
   :caption: Modules:
   :titlesonly:
   :maxdepth: 1
   :hidden:

   alphabet/aminoacid
   alphabet/gaps
   alphabet/nucleotide
'''


#    print listModules
#    print listFiles


###################
inDir = sys.argv[1]
outDir = sys.argv[2]


if len(sys.argv) > 3:
    testDir=sys.argv[3]
    generateIndex(inDir, outDir, True, testDir)
    generateRSTs(inDir, outDir,False)


else:
    generateIndex(inDir, outDir, False)
    generateRSTs(inDir, outDir, False)

